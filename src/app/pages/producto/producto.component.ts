import { Component, OnInit } from '@angular/core';
import { RepositoryApiService } from 'src/app/services/repository-api.service';
import { environment } from 'src/environments/environment';
//import { Producto } from 'src/app/Dto/producto';

@Component({
  selector: 'app-producto',
  templateUrl: './producto.component.html',
  styleUrls: ['./producto.component.css']
})
export class ProductoComponent implements OnInit {
  public productos: Producto[];
  constructor(private apiservice:RepositoryApiService) { }

  ngOnInit(): void {
    this.apiservice.uri = environment.uri_producto;
    this.apiservice.getData()
    .subscribe((data:Producto[]) => {
      this.productos = data;
        console.log()
    });  
  }

}


