import { Component, OnInit } from '@angular/core';
import { RepositoryApiService } from 'src/app/services/repository-api.service';
import { environment } from 'src/environments/environment';
//import { Cliente } from 'src/app/Dto/cliente';
@Component({
  selector: 'app-cliente',
  templateUrl: './cliente.component.html',
  styleUrls: ['./cliente.component.css']
})
export class ClienteComponent implements OnInit {
  public clientes: Cliente[];

  constructor(private apiservice:RepositoryApiService) {
  }

  ngOnInit(): void {
    this.apiservice.uri = environment.uri_cliente;
    this.apiservice.getData()
    .subscribe((data:Cliente[]) => {
      this.clientes = data;
        //sconsole.log()
    });  
    
  }

}
