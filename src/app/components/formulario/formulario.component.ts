import { Component, OnInit } from '@angular/core';
import { RepositoryApiService } from 'src/app/services/repository-api.service';
import { environment } from 'src/environments/environment';
import { Detalle } from 'src/app/Dto/detalle';

@Component({
  selector: 'app-formulario',
  templateUrl: './formulario.component.html',
  styleUrls: ['./formulario.component.css']
})
export class FormularioComponent implements OnInit {

  public detalle = new Detalle();
  public items = Array<Detalle>(); 
  public productos: Producto[];
  public detailProducto: Producto;
  public clientes: Cliente[];
  public total:number=0;
  public respuesta:boolean=false;
  public enviado:boolean=false;
  public nombrecliente:string;

  constructor(private apiservice:RepositoryApiService) { }

  ngOnInit(): void {
    this.apiservice.uri = environment.uri_producto;
    this.apiservice.getData()
    .toPromise()
    .then((data:Producto[])=> {
      //console.log(data);
      this.productos = data;
    })
    .catch((err)=> console.error(err)); 

    this.apiservice.uri = environment.uri_cliente;
    this.apiservice.getData()
    .toPromise()
    .then((data:Cliente[])=> {
      //console.log(data);
      this.clientes = data;
    })
    .catch((err)=> console.log(err));   

  }

  addItem(){
    let indexp= this.detalle.producto;

    this.detalle.id = this.productos[indexp].id;
    this.detalle.producto = this.productos[indexp].nombre;
    this.detalle.detalle = this.productos[indexp].descripcion;
    this.detalle.precio = this.productos[indexp].precio;
    this.detalle.subtotal = this.subtotal(parseInt(this.detalle.precio), parseInt(this.detalle.cantidad));
    this.items.push(this.detalle);

    this.detalle = new Detalle();

    let subtotales = this.items.map(function(data){
      return data.subtotal;
    });

    for(let i of subtotales) this.total+=i;
    console.log(this.items);
  }

  subtotal(precio:number, cantidad:number){
    return precio * cantidad;
  }

  sendFact(){
    this.enviado = true;
    this.apiservice.uri = environment.uri_factura;
    this.apiservice.addData<Array<Detalle>>(this.items)
    .toPromise()
    .then((data:any) => {
      this.respuesta  = data.estado;
      this.items.splice(0,this.items.length);
    })
    .catch((err)=> console.error(err)); 
  }

  removeitem(key:any){
    this.items.splice(key,1);
  }

  onChange(selectedId: number){
    let client =  this.clientes.find(c=>c.id==selectedId);
    this.nombrecliente = client.nombre + ' '+ client.apellido; 
  }
  
}
