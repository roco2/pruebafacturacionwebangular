import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule }   from '@angular/forms';

import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { RepositoryApiService } from './services/repository-api.service';

import { HomeComponent } from './pages/home/home.component';
import { ClienteComponent } from './pages/cliente/cliente.component';
import { ProductoComponent } from './pages/producto/producto.component';
import { FacturaComponent } from './pages/factura/factura.component';
import { NavMenuComponent } from './components/nav-menu/nav-menu.component';
import { FormularioComponent } from './components/formulario/formulario.component';


const appRoutes: Routes = [
  { path: '', component: HomeComponent, pathMatch: 'full' },
  { path: 'factura', component: FacturaComponent },
  { path: 'producto', component: ProductoComponent },
  { path: 'cliente', component: ClienteComponent },
];

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ClienteComponent,
    ProductoComponent,
    FacturaComponent,
    NavMenuComponent,
    FormularioComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot(
      appRoutes
    )
  ],
  providers: [RepositoryApiService],
  bootstrap: [AppComponent]
})
export class AppModule { }
