// interface Detalle {
//     producto: string;
//     detalle: string;
//     cantidad:string;
//     cliente:string;
//   }

  export class Detalle {
    constructor(
      public id: number=0,
      public producto:string='',      
      public detalle:string='',
      public cantidad:string='',
      public cliente:string='',
      public precio: string='',
      public subtotal: number=0
    ) {}
  }