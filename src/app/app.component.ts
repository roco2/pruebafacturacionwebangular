import { Component } from '@angular/core';
import { RepositoryApiService } from './services/repository-api.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  constructor() {
    
  }
  title = 'facturacionweb';

}
